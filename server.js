const express = require("express");
const app = express();
const PORT = process.env.PORT || 3000;

var stuff = [
    {
        id: 0,
        name: "something",
        boo: true
    },
    {
        id: 1,
        name: "another thing",
        boo: false
    }
];

app.get("/", function (req, res) {
    res.send("happy API");
});

// GET /stuff
app.get("/stuff", function (req, res) {
    res.json(stuff);
    // res.send(JSON.stringify(stuff));
});

// GET /stuff/:id
app.get("/stuff/:id", function (req, res) {
    var item = stuff.filter(function (item, index, list) {
       return item.id == req.params.id;
    });
    console.log(item);
    if (item[0]) {
        res.send(item);
    } else {
        res.status(404).send();
    }
});

app.listen(PORT, function () {
    console.log(`listening on port ${PORT}`);
});